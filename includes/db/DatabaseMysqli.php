<?php
/**
 * This is the MySQLi database abstraction layer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup Database
 */

require_once __DIR__ . "/../../sme_extn/sme_helper.php";

/**
 * Database abstraction object for PHP extension mysqli.
 *
 * @ingroup Database
 * @since 1.22
 * @see Database
 */
class DatabaseMysqli extends DatabaseMysqlBase {
	/** @var mysqli */
	protected $mConn;

	/**
	 * @param string $sql
	 * @return resource
	 */
	protected function doQuery( $sql ) {
		$conn = $this->getBindingHandle();

    #SME_CODE_BEGIN
    global $wgSMEEnabled, $wgSMEProfiling;
    if ( $wgSMEEnabled == 1 && $wgSMEProfiling == 1 ) {
      incrementDBQueryCount();
    }

    // debug code only
    if (isset($wgSMEEnabled) && isset($wgSMEDebug)
      && ($wgSMEEnabled && $wgSMEDebug)) {

      $sql_len = 0;
      if ($sql)
        $sql_len = strlen($sql);

      $today = date('Y-m-d H:i:s');
      $currtime = microtime(true);
      file_put_contents("/local/sme/myqueries.txt",
        "[$today $currtime] Q($sql_len): $sql\n", FILE_APPEND | LOCK_EX);
    }
    #SME_CODE_END

		if ( $this->bufferResults() ) {
			$ret = $conn->query( $sql );
		} else {
			$ret = $conn->query( $sql, MYSQLI_USE_RESULT );
		}


    #SME_CODE_BEGIN
    // debug code only
    if (isset($wgSMEEnabled) && isset($wgSMEDebug)
      && ($wgSMEEnabled && $wgSMEDebug)) {

      $today = date('Y-m-d H:i:s');
      $currtime = microtime(true);
      $res_str = print_r($ret, true);
      file_put_contents("/local/sme/myqueries.txt",
        "[$today $currtime] R: $res_str\n", FILE_APPEND | LOCK_EX);
    }
    #SME_CODE_END

		return $ret;
	}

	/**
	 * @param string $realServer
	 * @return bool|mysqli
	 * @throws DBConnectionError
	 */
	protected function mysqlConnect( $realServer ) {
		global $wgDBmysql5;
    global $wgSMEMultiTier;
		# Avoid suppressed fatal error, which is very hard to track down
		if ( !function_exists( 'mysqli_init' ) ) {
			throw new DBConnectionError( $this, "MySQLi functions missing,"
				. " have you compiled PHP with the --with-mysqli option?\n" );
		}

		// Other than mysql_connect, mysqli_real_connect expects an explicit port
		// and socket parameters. So we need to parse the port and socket out of
		// $realServer
		$port = null;
		$socket = null;
		$hostAndPort = IP::splitHostAndPort( $realServer );
		if ( $hostAndPort ) {
			$realServer = $hostAndPort[0];
			if ( $hostAndPort[1] ) {
				$port = $hostAndPort[1];
			}
		} elseif ( substr_count( $realServer, ':' ) == 1 ) {
			// If we have a colon and something that's not a port number
			// inside the hostname, assume it's the socket location
			$hostAndSocket = explode( ':', $realServer );
			$realServer = $hostAndSocket[0];
			$socket = $hostAndSocket[1];
		}

		$connFlags = 0;
		if ( $this->mFlags & DBO_SSL ) {
			$connFlags |= MYSQLI_CLIENT_SSL;
		}
		if ( $this->mFlags & DBO_COMPRESS ) {
			$connFlags |= MYSQLI_CLIENT_COMPRESS;
		}
		if ( $this->mFlags & DBO_PERSISTENT ) {
			$realServer = 'p:' . $realServer;
		}

		$mysqli = mysqli_init();
		if ( $wgDBmysql5 ) {
			// Tell the server we're communicating with it in UTF-8.
			// This may engage various charset conversions.
			$mysqli->options( MYSQLI_SET_CHARSET_NAME, 'utf8' );
		} else {
			$mysqli->options( MYSQLI_SET_CHARSET_NAME, 'binary' );
		}
		$mysqli->options( MYSQLI_OPT_CONNECT_TIMEOUT, 3 );
		
		if ( $mysqli->real_connect( $realServer, $this->mUser,
			$this->mPassword, $this->mDBname, $port, $socket, $connFlags )
		) {
      global $wgSMEEnabled, $wgSMEEnforcement;
      global $public_input_hash;
      global $private_input_hash;
      if (isset($wgSMEEnabled) && isset($wgSMEEnforcement) && isset($wgSMEMultiTier)
        && $wgSMEEnabled == 1 && $wgSMEEnforcement == 1 && $wgSMEMultiTier == 1) {
        sendBackendProfileToKernel($public_input_hash, $private_input_hash, $mysqli);
      }
      return $mysqli;
		}
		return false;
	}

	protected function connectInitCharset() {
		// already done in mysqlConnect()
		return true;
	}

	/**
	 * @param string $charset
	 * @return bool
	 */
	protected function mysqlSetCharset( $charset ) {
		$conn = $this->getBindingHandle();

		if ( method_exists( $conn, 'set_charset' ) ) {
			return $conn->set_charset( $charset );
		} else {
			return $this->query( 'SET NAMES ' . $charset, __METHOD__ );
		}
	}

	/**
	 * @return bool
	 */
	protected function closeConnection() {
		$conn = $this->getBindingHandle();

		#file_put_contents('/local/sme/mediawiki_request_logs.txt'," I am Closing the Connection, my conn is: $conn\n",  FILE_APPEND | LOCK_EX);
		return $conn->close();
	}

	/**
	 * @return int
	 */
	function insertId() {
		$conn = $this->getBindingHandle();

		return (int)$conn->insert_id;
	}

	/**
	 * @return int
	 */
	function lastErrno() {
		if ( $this->mConn ) {
			return $this->mConn->errno;
		} else {
			return mysqli_connect_errno();
		}
	}

	/**
	 * @return int
	 */
	function affectedRows() {
		$conn = $this->getBindingHandle();

		return $conn->affected_rows;
	}

	/**
	 * @param string $db
	 * @return bool
	 */
	function selectDB( $db ) {
		$conn = $this->getBindingHandle();

		$this->mDBname = $db;

		return $conn->select_db( $db );
	}

	/**
	 * @param mysqli $res
	 * @return bool
	 */
	protected function mysqlFreeResult( $res ) {
		$res->free_result();

		return true;
	}

	/**
	 * @param mysqli $res
	 * @return bool
	 */
	protected function mysqlFetchObject( $res ) {
		$object = $res->fetch_object();
		if ( $object === null ) {
			return false;
		}

		return $object;
	}

	/**
	 * @param mysqli $res
	 * @return bool
	 */
	protected function mysqlFetchArray( $res ) {
		$array = $res->fetch_array();
		if ( $array === null ) {
			return false;
		}

		return $array;
	}

	/**
	 * @param mysqli $res
	 * @return mixed
	 */
	protected function mysqlNumRows( $res ) {
		return $res->num_rows;
	}

	/**
	 * @param mysqli $res
	 * @return mixed
	 */
	protected function mysqlNumFields( $res ) {
		return $res->field_count;
	}

	/**
	 * @param mysqli $res
	 * @param int $n
	 * @return mixed
	 */
	protected function mysqlFetchField( $res, $n ) {
		$field = $res->fetch_field_direct( $n );

		// Add missing properties to result (using flags property)
		// which will be part of function mysql-fetch-field for backward compatibility
		$field->not_null = $field->flags & MYSQLI_NOT_NULL_FLAG;
		$field->primary_key = $field->flags & MYSQLI_PRI_KEY_FLAG;
		$field->unique_key = $field->flags & MYSQLI_UNIQUE_KEY_FLAG;
		$field->multiple_key = $field->flags & MYSQLI_MULTIPLE_KEY_FLAG;
		$field->binary = $field->flags & MYSQLI_BINARY_FLAG;
		$field->numeric = $field->flags & MYSQLI_NUM_FLAG;
		$field->blob = $field->flags & MYSQLI_BLOB_FLAG;
		$field->unsigned = $field->flags & MYSQLI_UNSIGNED_FLAG;
		$field->zerofill = $field->flags & MYSQLI_ZEROFILL_FLAG;

		return $field;
	}

	/**
	 * @param resource|ResultWrapper $res
	 * @param int $n
	 * @return mixed
	 */
	protected function mysqlFieldName( $res, $n ) {
		$field = $res->fetch_field_direct( $n );

		return $field->name;
	}

	/**
	 * @param resource|ResultWrapper $res
	 * @param int $n
	 * @return mixed
	 */
	protected function mysqlFieldType( $res, $n ) {
		$field = $res->fetch_field_direct( $n );

		return $field->type;
	}

	/**
	 * @param resource|ResultWrapper $res
	 * @param int $row
	 * @return mixed
	 */
	protected function mysqlDataSeek( $res, $row ) {
		return $res->data_seek( $row );
	}

	/**
	 * @param mysqli $conn Optional connection object
	 * @return string
	 */
	protected function mysqlError( $conn = null ) {
		if ( $conn === null ) {
			return mysqli_connect_error();
		} else {
			return $conn->error;
		}
	}

	/**
	 * Escapes special characters in a string for use in an SQL statement
	 * @param string $s
	 * @return string
	 */
	protected function mysqlRealEscapeString( $s ) {
		$conn = $this->getBindingHandle();

		return $conn->real_escape_string( $s );
	}

	protected function mysqlPing() {
		$conn = $this->getBindingHandle();

		return $conn->ping();
	}

	/**
	 * Give an id for the connection
	 *
	 * mysql driver used resource id, but mysqli objects cannot be cast to string.
	 * @return string
	 */
	public function __toString() {
		if ( $this->mConn instanceof mysqli ) {
			return (string)$this->mConn->thread_id;
		} else {
			// mConn might be false or something.
			return (string)$this->mConn;
		}
	}
}
