<?php

include_once("/local/sme/workspace/side-channels/enforcement/php-ioctl/phpioctl.php");

function get_local_ip_port(&$local_ip, &$local_port)
{
  $local_ip = $_SERVER['SERVER_ADDR'];
  $local_port = $_SERVER['SERVER_PORT'];
}

function get_remote_ip_port(&$remote_ip, &$remote_port)
{
  if (!isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      $remote_ip = $_SERVER['REMOTE_ADDR'];
      $remote_port = $_SERVER['REMOTE_PORT'];
  } else {
      $remote_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
      $remote_port = $_SERVER['REMOTE_PORT'];
  }
}

function identify_backend_connection(&$local_ip, &$local_port,
  &$remote_ip, &$remote_port)
{
  global $l_output;
  $pid = getmypid();
  #get all socket file descriptors of this process
  $files1 = scandir("/proc/$pid/fd");
  foreach($files1 as $value) {
    $lsr = readlink("/proc/$pid/fd/$value");
    if (strpos($lsr, 'socket') !== false) {
      $lsr = explode("[", explode("]", $lsr)[0])[1];
      if (!in_array($lsr, $fds)) {
        $fds[] = print_r($lsr, true);
      }
    }
  }

  $lines1 = file("/proc/net/tcp", FILE_IGNORE_NEW_LINES);

  foreach($lines1 as $value) {
    // always remove leading (and sometimes even trailing) spaces
    $value = trim($value, " ");
    $vlist = preg_split('/\s+/', $value);
    // split line at spaces, and take the 10th column
    $inode = $vlist[9];
    // split again at spaces, take the 2nd and 3rd column, further split with ":"
    $local_address = explode(":", $vlist[1]);
    $remote_address = explode(":", $vlist[2]);
    // convert address[0] from 4 ints in hex to string
    $l_ip = implode(".",array_reverse(explode(".",long2ip("0x$local_address[0]"))));
    // port = address[1]
    $l_port = hexdec($local_address[1]);
    $r_ip = implode(".",array_reverse(explode(".",long2ip("0x$remote_address[0]"))));
    $r_port = hexdec($remote_address[1]);
    $socket_info = "$l_ip:$l_port -> $r_ip:$r_port";

    if(in_array($inode, $fds) && !in_array($socket_info, $l_output)) {
      $l_output[] = print_r($socket_info, true);
      $local_ip = $l_ip;
      $local_port = $l_port;
      $remote_ip = $r_ip;
      $remote_port= $r_port;
    }
  }
}

/**
 * packing functions
 */
class int_helper
{
    public static function int8($i)
    {
      return is_int($i) ? pack("c", $i) : unpack("c", $i)[1];
    }

    public static function uInt8($i)
    {
      return is_int($i) ? pack("C", $i) : unpack("C", $i)[1];
    }

    public static function int16($i)
    {
      return is_int($i) ? pack("s", $i) : unpack("s", $i)[1];
    }

    public static function uInt16($i, $endianness = null)
    {
      $f = is_int($i) ? "pack" : "unpack";

      if ($endianness === true) {  // big-endian
          $i = $f("n", $i);
      } else if ($endianness === false) {  // little-endian
          $i = $f("v", $i);
      } else if ($endianness === null) {  // machine byte order
          $i = $f("S", $i);
      }

      return is_array($i) ? $i[1] : $i;
    }

    public static function int32($i)
    {
      return is_int($i) ? pack("l", $i) : unpack("l", $i)[1];
    }

    public static function uInt32($i, $endianness = null)
    {
      $f = is_int($i) ? "pack" : "unpack";

      if ($endianness === true) {  // big-endian
          $i = $f("N", $i);
      } else if ($endianness === false) {  // little-endian
          $i = $f("V", $i);
      } else if ($endianness === null) {  // machine byte order
          $i = $f("L", $i);
      }

      return is_array($i) ? $i[1] : $i;
    }

    public static function int64($i)
    {
      return is_int($i) ? pack("q", $i) : unpack("q", $i)[1];
    }

    public static function uInt64($i, $endianness = null)
    {
      $f = is_int($i) ? "pack" : "unpack";

      if ($endianness === true) {  // big-endian
          $i = $f("J", $i);
      } else if ($endianness === false) {  // little-endian
          $i = $f("P", $i);
      } else if ($endianness === null) {  // machine byte order
          $i = $f("Q", $i);
      }

      return is_array($i) ? $i[1] : $i;
    }
}

/**
 * Number of queries during this session,
 * this should probably be moved to context
 */
$dbQueryCount = 0;

function getDBQueryCount()
{
  global $dbQueryCount;
    return $dbQueryCount;
}

function incrementDBQueryCount()
{
  global $dbQueryCount;
  global $fds;
  $dbQueryCount = $dbQueryCount + 1;
}

/**
 * profile helper class
 */
class Profile
{
    public $prof_id;
    public $num_out_reqs;
    public $num_extra_slots;
    public $num_out_frames;
    public $frame_spacing;
    public $first_frame_latency;

    public function __construct($prof_id, $num_out_reqs, $num_extra_slots,
      $num_out_frames, $frame_spacing, $first_frame_latency)
    {
      $this->prof_id = $prof_id;
      $this->num_out_reqs = $num_out_reqs;
      $this->num_extra_slots = $num_extra_slots;
      $this->num_out_frames = $num_out_frames;
      $this->frame_spacing = $frame_spacing;
      $this->first_frame_latency = $first_frame_latency;
    }

    public function prepare_binary_profile()
    {
      $prof_id = $this->prof_id;
      $num_out_reqs = $this->num_out_reqs;
      $num_extra_slots = $this->num_extra_slots;
      $num_out_frames = $this->num_out_frames;
      $frame_spacing = $this->frame_spacing;
      $first_frame_latency = $this->first_frame_latency;

      $binarydata = "";
      $binarydata .= int_helper::uInt16($prof_id, false);
      $binarydata .= int_helper::uInt16($num_out_reqs, false);
      for ($i = 0; $i < $num_out_reqs; $i++) {
        $binarydata .= int_helper::uInt16($num_extra_slots[$i], false);
      }
      for ($i = 0; $i < $num_out_reqs; $i++) {
        $binarydata .= int_helper::uInt16($num_out_frames[$i], false);
      }
      for ($i = 0; $i < $num_out_reqs; $i++) {
        $binarydata .= int_helper::uInt64($frame_spacing[$i], false);
      }
      for ($i = 0; $i < $num_out_reqs; $i++) {
        $binarydata .= int_helper::uInt64($first_frame_latency[$i], false);
      }
      $this->binary_profile = $binarydata;
    }

    public function dump_profile()
    {
      $pbuf = "";
      $pbuf .= "profile id: $this->prof_id";
      $pbuf .= ", #reqs: $this->num_out_reqs\n";
      $pbuf .= "extra slots ---\n";
      for ($i = 0; $i < $this->num_out_reqs; $i++) {
        $pbuf .= $this->num_extra_slots[$i];
        $pbuf .= " ";
      }
      $pbuf .= "\n";

      $pbuf .= "num out frames ---\n";
      for ($i = 0; $i < $this->num_out_reqs; $i++) {
        $pbuf .= $this->num_out_frames[$i];
        $pbuf .= " ";
      }
      $pbuf .= "\n";

      $pbuf .= "spacing ---\n";
      for ($i = 0; $i < $this->num_out_reqs; $i++) {
        $pbuf .= $this->frame_spacing[$i];
        $pbuf .= " ";
      }
      $pbuf .= "\n";

      $pbuf .= "latency ---\n";
      for ($i = 0; $i < $this->num_out_reqs; $i++) {
        $pbuf .= $this->first_frame_latency[$i];
        $pbuf .= " ";
      }
      $pbuf .= "\n";

      file_put_contents("/local/sme/profile" . $this->prof_id . ".txt",
        $pbuf . "\n", FILE_APPEND|LOCK_EX);
    }
}

/**
 * profile related functions
 */

define('PROF_CONN_IP_HDR_LEN', 16);
define('PROF_CONN_PORT_HDR_LEN', 8);
define('PROF_CONN_IP_PORT_HDR_LEN', 24);
define('MSG_HDR_LEN', 16);

define('MSG_T_IPPORT', 0);
define('MSG_T_PROFILE', 1);
define('MSG_T_PMAP', 2);
define('MSG_T_PROF_ID', 3);
define('MSG_T_MARKER', 4);
define('MSG_T_DATA', 5);

define('HASH_LEN', 16);

function PROF_CONN_PMAP_HDR_LEN($n_prof)
{
  $len = 2 + ($n_prof * 2);
  return $len;
}

function PROF_CONN_VAR_LEN_BUF_LEN($n_req)
{
  $len = ($n_req * 2) + (($n_req * 8) * 3);
  return $len;
}

function PROF_CONN_LEN($n_req)
{
  $len = (2 * 2) + PROF_CONN_VAR_LEN_BUF_LEN($n_req);
  return $len;
}

function prepare_n_profile_map_from_buf(&$pbuf, &$pbuf_len, $n_profile,
  $src_ip, $in_ip, $out_ip, $dst_ip, $src_port, $in_port, $out_port, $dst_port,
  $prof_buf, $prof_buf_len)
{

  $off2 = 0;
  $total_len = 0;

  $pbuf = "";
  $pbuf .= int_helper::uInt32($src_ip, true);
  $pbuf .= int_helper::uInt32($in_ip, true);
  $pbuf .= int_helper::uInt32($out_ip, true);
  $pbuf .= int_helper::uInt32($dst_ip, true);
  $off += 4*4;

  $pbuf .= int_helper::uInt16($src_port, true);
  $pbuf .= int_helper::uInt16($in_port, true);
  $pbuf .= int_helper::uInt16($out_port, true);
  $pbuf .= int_helper::uInt16($dst_port, true);
  $off += 4*2;

  $pbuf .= int_helper::uInt16($n_profile, false);
  $off += 2;

  $total_len = $off + ($n_profile * 2);
  for ($i = 0; $i < $n_profile; $i++) {
    $pbuf .= int_helper::uInt16($total_len, false);
    $total_len += $prof_buf_len[$i];
  }
  $off += ($n_profile * 2);

  $off2 = 0;
  for ($i = 0; $i < $n_profile; $i++) {
    $pbuf .= $prof_buf[$i];
    $off2 += $prof_buf_len[$i];
  }

  $pbuf_len = $off + $off2;
}

function create_tmp_profile_map(&$buf, &$buf_len, $server_ip, $server_port,
  $client_ip, $client_port, $n_prof, $fname, $read)
{
  $num_out_reqs = array();
  $prof_len = array();
  $prof_buf = array();
  $total_msg_len = 0;
  for ($i = 0; $i < $n_prof; $i++) {
    $num_req = rand(1,5);
    $plen = PROF_CONN_LEN($num_req);
    $num_out_reqs[] = $num_req;
    $prof_len[] = $plen;
    $total_msg_len += $plen;
  }
  $total_msg_len += PROF_CONN_IP_HDR_LEN;
  $total_msg_len += PROF_CONN_PORT_HDR_LEN;
  $total_msg_len += PROF_CONN_PMAP_HDR_LEN($n_prof);
  $total_msg_len += MSG_HDR_LEN;

  for ($i = 0; $i < $n_prof; $i++) {
    $num_extra_slots = [];
    $num_out_frames = [];
    $spacing = [];
    $latency = [];

    for ($j = 0; $j < $num_out_reqs[$i]; $j++) {
      $num_extra_slots[] = rand(0,5);
      $num_out_frames[] = rand(1,10);
      $spacing[] = rand(1,7)*100000000;
      $latency[] = rand(1,20)*100000000;
    }
    $p = new Profile($i, $num_out_reqs[$i], $num_extra_slots, $num_out_frames,
      $spacing, $latency);
    $p->dump_profile();
    $p->prepare_binary_profile();
    $prof_buf[] = $p->binary_profile;
  }

  // packing done in the prepare_n_profile_map_from_buf function
  $client_ip_long = ip2long($client_ip);
  $server_ip_long = ip2long($server_ip);
  $client_port_int = intval($client_port);
  $server_port_int = intval($server_port);

  $pmap = "";
  prepare_n_profile_map_from_buf($pmap, $pmap_len, $n_prof,
    $client_ip_long, $server_ip_long, $server_ip_long, $client_ip_long,
    $client_port_int, $server_port_int, $server_port_int, $client_port_int,
    $prof_buf, $prof_len);

  $msg = "";
  $msg .= int_helper::uInt64(MSG_T_PMAP, false);
  $msg .= int_helper::uInt64($pmap_len, false);

  $buf = $msg . $pmap;
  $buf_len = $total_msg_len;

  //file_put_contents($fname, $pmap . "\n", FILE_APPEND | LOCK_EX);
  file_put_contents($fname,
    "msg type: " . MSG_T_PMAP . ", payload len: $pmap_len, total msg len: $buf_len"
    . ", server ip: $server_ip, " . ip2long($server_ip)
    . ", " . bin2hex($server_ip_long)
    . ", " . bin2hex(int_helper::uInt32(ip2long($server_ip), true))
    . ", client ip: $client_ip, " . ip2long($client_ip)
    . ", " . bin2hex($client_ip_long)
    . ", " . bin2hex(int_helper::uInt32(ip2long($client_ip), true))
    . ", server port: $server_port, client port: $client_port"
    . "\n",
    FILE_APPEND|LOCK_EX);
  file_put_contents($fname, bin2hex($pmap) . "\n" . bin2hex($msg) . "\n" . bin2hex($buf) . "\n",
    FILE_APPEND | LOCK_EX);
}

function create_tmp_profile_id(&$buf, &$buf_len, $src_ip, $src_port,
  $in_ip, $in_port, $out_ip, $out_port, $dst_ip, $dst_port, $prof_id)
{
  global $wgSMEDebug;
  $prof_buf = "";
  $prof_buf .= int_helper::uInt32(ip2long($src_ip), true);
  $prof_buf .= int_helper::uInt32(ip2long($in_ip), true);
  $prof_buf .= int_helper::uInt32(ip2long($out_ip), true);
  $prof_buf .= int_helper::uInt32(ip2long($dst_ip), true);

  $prof_buf .= int_helper::uInt16(intval($src_port), true);
  $prof_buf .= int_helper::uInt16(intval($in_port), true);
  $prof_buf .= int_helper::uInt16(intval($out_port), true);
  $prof_buf .= int_helper::uInt16(intval($dst_port), true);

  $prof_buf .= int_helper::uInt16($prof_id, false);

  $plen = PROF_CONN_IP_PORT_HDR_LEN + 2;

  $msg_type = MSG_T_PROF_ID;

  $msg = "";
  $msg .= int_helper::uInt64($msg_type, false);
  $msg .= int_helper::uInt64($plen, false);

  $buf = $msg . $prof_buf;
  $buf_len = MSG_HDR_LEN + $plen;

  $dstr = "";
  $dstr .= "msg type: $msg_type";
  $dstr .= ", len: $plen";
  $dstr .= ", src ip: $src_ip";
  $dstr .= ", src port: $src_port";
  $dstr .= ", in ip: $in_ip";
  $dstr .= ", in port: $in_port";
  $dstr .= ", out ip: $out_ip";
  $dstr .= ", out port: $out_port";
  $dstr .= ", dst ip: $dst_ip";
  $dstr .= ", dst port: $dst_port";
  $dstr .= ", prof id: $prof_id";

  if (isset($wgSMEDebug) && $wgSMEDebug) {
    file_put_contents("/local/sme/msg_sent_to_kernel.txt",
      "BINARY === " . bin2hex($prof_buf) . "\nSTRING === $dstr\n", LOCK_EX);
  }

  /*
  if (isset($wgSMEDebug) && $wgSMEDebug) {
    file_put_contents("/local/sme/udp.txt",
      "SRC: $src_ip, $src_port, IN: $in_ip, $in_port\n" .
      "OUT: $out_ip, $out_port, DST: $dst_ip, $dst_port\n",
      FILE_APPEND|LOCK_EX);
  }
   */
}

$SME_MARKER_SOCK_OR_CTL = -1;
$SME_REQUEST_TIMESTAMP = -1;
$SME_KERNEL_ENFORCEMENT_TIMESTAMP = -1;
$SME_REQUEST_ID = -1;
$public_input_hash = array();
$private_input_hash = array();

function filesize_binning_wiktionary($file_size)
{
  /*
   * #frames in profiles: S - 21, M - 21, L - 21, XL - 401
   */
  if (intval($file_size) <= 6000)
    return "small";

  if (intval($file_size) <= 9000)
    return "medium";

  if (intval($file_size) <= 12000)
    return "large";

  return "extra-large";
}

function filesize_binning_wiktionary_unzipped($file_size)
{
  /*
   * #frames in profiles: S - 21, M - 21, L - 64, XL - 3879 --> 4363
   */
#  if (intval($file_size) <= 14600)
#    return "small";

  if (intval($file_size) <= 24820)
    return "medium";

  if (intval($file_size) <= 81760)
    return "large";

  return "extra-large";
}

function filesize_binning_medical_wiktionary($file_size)
{
  /*
   * superimposing medical dataset onto wiktionary dataset.
   * clustered medical dataset into 3 clusters: M, L, XL.
   * ceilings: M - 70952, L - 85432, XL - 485080
   * #files (medical) in each cluster: M - 2300, L - 2373, XL - 2205
   * #frames in profiles (including ssl overhead): S - 20, M - 53, L - 64, XL - 362
   * medical trace workload will never invoke S cluster.
   */
  if (intval($file_size) <= 6000)
    return "small";

  if ((intval($file_size) > 6000 && intval($file_size) <= 9000)
    || (intval($file_size) > 49348 && intval($file_size) <= 70952))
    return "medium";

  if ((intval($file_size) > 9000 && intval($file_size) <= 12000)
    || (intval($file_size) > 70952 && intval($file_size) <= 85432))
    return "large";

  if ((intval($file_size) > 12000 && intval($file_size) <= 49348)
    || (intval($file_size) > 85432))
    return "extra-large";
}


function set_request_id($pubinput, $privinput)
{
  global $SME_REQUEST_TIMESTAMP;
  global $SME_KERNEL_ENFORCEMENT_TIMESTAMP;
  global $SME_REQUEST_ID;
  global $public_input_hash;
  global $private_input_hash;
  global $wgSMEPrivBinning;
  global $wgFileCacheDirectory;
  global $wgSMECPUUtilMarker;
  $num_pub = count($pubinput);
  $num_priv = count($privinput);
  /*
   * set default to 100, so that if CPU% based profile scaling is disabled,
   * even if we accidentally forget to adjust profiles, we at least use the
   * most over-provisioned profile without breaking the experiment.
   */
  $cpu_load = 100;

  if (isset($wgSMECPUUtilMarker) && $wgSMECPUUtilMarker == 1) {
    $filename = "/mnt/cpu_util/cpu_util.txt";
    $handle = fopen($filename, "r");
    $cpu_load = fread($handle, 4);

    if ($cpu_load <= 10) {
      $cpu_load = 10;
    } elseif ($cpu_load <= 50) {
      $cpu_load = 50;
    } elseif ($cpu_load <= 80) {
      $cpu_load = 80;
    } else {
      $cpu_load = 100;
    }

    #echo $cpu_load;
    fclose($handle);
  }

  #print_r($pubinput);
  #print_r($privinput);
  for ($i = 0; $i < $num_pub; $i++) {

    $cluster_name = $cpu_load . $pubinput[$i];
    $pubinput[$i] = $cpu_load . $pubinput[$i];

    if (isset($wgSMEPrivBinning) && $wgSMEPrivBinning == 1) {
      $hash = md5($privinput[$i]);
      if (str_contains($wgFileCacheDirectory, "padded") == True) {
        $sz_fpath = $wgFileCacheDirectory;
        $sz_fpath .= '/'.substr($hash,0,1).'/'.substr($hash,0,2);
        $sz_fpath .= '/'.urlencode($privinput[$i]).'.html';
        $file_size = filesize($sz_fpath);
      } else {
        $sz_fpath = $wgFileCacheDirectory;
        $sz_fpath .= '/'.substr($hash,0,1).'/'.substr($hash,0,2);
        $sz_fpath .= '/'.urlencode($privinput[$i]).'.html.gz';
        $file_size = filesize($sz_fpath);
      }
      $cluster_suff = filesize_binning_wiktionary(intval($file_size));
#      $cluster_suff = filesize_binning_wiktionary_unzipped(intval($file_size));
      $cluster_name = $cluster_name . $cluster_suff;
      $public_input_hash[] = md5($cluster_name, True);
    } else {
      $public_input_hash[] = md5($pubinput[$i], True);
    }

  }

  for ($i = 0; $i < $num_priv; $i++) {
    $private_input_hash[] =  md5($privinput[$i], True);
  }

  #print_r($public_input_hash);

  // request ID
  $reqID1 = str_repeat('0', HASH_LEN/2);
  $reqID2 = str_repeat('0', HASH_LEN/2);

  if ($num_pub > 0)
    $reqID1 = substr($public_input_hash[0], 0, HASH_LEN/2);
  if ($num_priv > 0)
    $reqID2 = substr($private_input_hash[$num_priv-1], 0, HASH_LEN/2);

  $SME_REQUEST_TIMESTAMP = intval(microtime(True)*1000000000);

  $SME_REQUEST_ID = md5($SME_REQUEST_TIMESTAMP . $reqID1 . $reqID2, True);
  #to make sure the request timestamp will be used if enforcement is not running
  $SME_KERNEL_ENFORCEMENT_TIMESTAMP = -1;
}


function create_tmp_profile_marker(&$buf, &$buf_len, $src_ip, $src_port,
  $in_ip, $in_port, $out_ip, $out_port, $dst_ip, $dst_port,
  $num_pub, $num_priv, $pubhash, $privhash)
{
  global $wgSMEDebug;
  global $SME_REQUEST_TIMESTAMP;
  global $SME_REQUEST_ID;
  global $SME_KERNEL_ENFORCEMENT_TIMESTAMP;

  $SME_MARKER_TIMESTAMP = $SME_KERNEL_ENFORCEMENT_TIMESTAMP;
  if ($SME_MARKER_TIMESTAMP == -1) {
    $SME_MARKER_TIMESTAMP = $SME_REQUEST_TIMESTAMP;
  }

  #if (isset($wgSMEDebug) && $wgSMEDebug) {
  # file_put_contents("/local/sme/msg_sent_to_kernel.txt",
  #   "SME MICROTIME TIMESTAMP: $SME_REQUEST_TIMESTAMP\n", LOCK_EX|FILE_APPEND);
  # file_put_contents("/local/sme/msg_sent_to_kernel.txt",
  #   "SME MARKER TIMESTAMP: $SME_MARKER_TIMESTAMP\n", LOCK_EX|FILE_APPEND);
  # file_put_contents("/local/sme/msg_sent_to_kernel.txt",
  #   "SME KERNEL ENFORCEMENT TIMESTAMP: $SME_KERNEL_ENFORCEMENT_TIMESTAMP\n", LOCK_EX|FILE_APPEND);
  #}

  $prof_buf = "";
  if ($src_ip != "")
    $prof_buf .= int_helper::uInt32(ip2long($src_ip), true);
  else
    $prof_buf .= int_helper::uInt32(ip2long("0.0.0.0"), true);

  if ($in_ip != "")
    $prof_buf .= int_helper::uInt32(ip2long($in_ip), true);
  else
    $prof_buf .= int_helper::uInt32(ip2long("0.0.0.0"), true);

  if ($out_ip != "")
    $prof_buf .= int_helper::uInt32(ip2long($out_ip), true);
  else
    $prof_buf .= int_helper::uInt32(ip2long("0.0.0.0"), true);

  if ($dst_ip != "")
    $prof_buf .= int_helper::uInt32(ip2long($dst_ip), true);
  else
    $prof_buf .= int_helper::uInt32(ip2long("0.0.0.0"), true);

  $prof_buf .= int_helper::uInt16(intval($src_port), true);
  $prof_buf .= int_helper::uInt16(intval($in_port), true);
  $prof_buf .= int_helper::uInt16(intval($out_port), true);
  $prof_buf .= int_helper::uInt16(intval($dst_port), true);

  $prof_buf .= int_helper::uInt32(intval($num_pub), false);
  $prof_buf .= int_helper::uInt32(intval($num_priv), false);

  // request ID
  #$reqID1 = str_repeat('0', HASH_LEN/2);
  #$reqID2 = str_repeat('0', HASH_LEN/2);
  #if ($num_pub > 0)
  # $reqID1 = substr($pubhash[0], 0, HASH_LEN/2);
  #if ($num_priv > 0)
  # $reqID2 = substr($privhash[$num_priv-1], 0, HASH_LEN/2);

  #$prof_buf .= $reqID1 . $reqID2;
  #if ($SME_REQUEST_TIMESTAMP == -1) {
  #  $SME_REQUEST_TIMESTAMP = microtime();
  #}
  #if ($SME_REQUEST_ID == -1) {
  #  $SME_REQUEST_ID = md5($SME_REQUEST_TIMESTAMP . $reqID1 . $reqID2, True);
  #}

  $prof_buf .=  $SME_REQUEST_ID;
  #print_r($SME_REQUEST_ID);

  // marker timestamp, may be used by the kernel
  $prof_buf .=  int_helper::uInt64($SME_MARKER_TIMESTAMP, false);

  // #pub hashes
  for ($i = 0; $i < $num_pub; $i++) {
    #print_r($pubhash[$i]);
    $prof_buf .= $pubhash[$i];
  }

  // #priv hashes
  for ($i = 0; $i < $num_priv; $i++) {
    #print_r($privhash[$i]);
    $prof_buf .= $privhash[$i];
  }

  $plen = PROF_CONN_IP_PORT_HDR_LEN + 2*4 + HASH_LEN + 8
    + ($num_pub + $num_priv) * HASH_LEN;

  $msg_type = MSG_T_MARKER;

  $msg = "";
  $msg .= int_helper::uInt64($msg_type, false);
  $msg .= int_helper::uInt64($plen, false);

  $buf = $msg . $prof_buf;
  $buf_len = MSG_HDR_LEN + $plen;

  if (isset($wgSMEDebug) && $wgSMEDebug) {
    $dstr = "";
    $dstr .= "msg type: $msg_type";
    $dstr .= ", len: $plen";
    $dstr .= ", src ip: $src_ip";
    $dstr .= ", src port: $src_port";
    $dstr .= ", in ip: $in_ip";
    $dstr .= ", in port: $in_port";
    $dstr .= ", out ip: $out_ip";
    $dstr .= ", out port: $out_port";
    $dstr .= ", dst ip: $dst_ip";
    $dstr .= ", dst port: $dst_port";
    $dstr .= ", num pub: $num_pub";
    $dstr .= ", num priv: $num_priv";
    if ($num_pub > 0) {
      $pubhash1 = bin2hex($pubhash[0]);
      $dstr .= ", pub_hash1: $pubhash1";
    }
    $ts = bin2hex(int_helper::uInt64($SME_MARKER_TIMESTAMP, false));
    $dstr .= ", TS $ts TIMESTAMP VALUE: $SME_MARKER_TIMESTAMP";
    $reqid = bin2hex($SME_REQUEST_ID);
    $dstr .= ", REQUEST_ID: $reqid";
    $hstr = bin2hex($prof_buf);
    $hlen = strlen($hstr);

    file_put_contents("/local/sme/msg_sent_to_kernel.txt",
      "BINARY[$hlen] === $hstr\nSTRING === $dstr\n", LOCK_EX|FILE_APPEND);
  }

  /*
  if (isset($wgSMEDebug) && $wgSMEDebug) {
    file_put_contents("/local/sme/udp.txt",
      "SRC: $src_ip, $src_port, IN: $in_ip, $in_port\n" .
      "OUT: $out_ip, $out_port, DST: $dst_ip, $dst_port\n" .
      "plen: $plen, buflen: $buf_len\n",
      FILE_APPEND|LOCK_EX);
  }
   */
}

/**
 * profile related API
 */

function appendProfileLog($action, $title)
{
  global $l_output;
  global $wgEnableSMELogging;

  if (!isset($wgEnableSMELogging) || $wgEnableSMELogging != 1) {
    return;
  }
  get_local_ip_port($server_ip, $server_port);
  get_remote_ip_port($client_ip, $client_port);
  $client_action = ($action == "" ? "view" : $action);
  $query_count = getDBQueryCount();

  file_put_contents('/local/sme/mediawiki_request_logs.txt',
    "$client_ip:$client_port -> $server_ip:$server_port\t$client_action\t$title\t$query_count\n",
    FILE_APPEND | LOCK_EX);
  file_put_contents('/local/sme/server_to_database_splits.txt',
    "$query_count\n",  FILE_APPEND | LOCK_EX);

  #identify_backend_connection($a,$b,$c,$d);
  $final_l_output = print_r($l_output, true);
  file_put_contents("/local/sme/mediawiki_request_logs.txt",
    "$final_l_output\n", FILE_APPEND | LOCK_EX);
}

function sendBackendProfileToBackendMysql($pbuf_BE_FE, $plen_BE_FE, $mysqli,
  $dummy_server_ip, $dummy_server_port)
{
  #$sql_str = "select udpsock(\"" . $pbuf_BE_FE . "\", $plen_BE_FE,
  $sql_str = "select udpsock(\"" . bin2hex($pbuf_BE_FE) . "\", $plen_BE_FE,
    '$dummy_server_ip', $dummy_server_port) from dual;";

  $result = $mysqli->query($sql_str);
  if (!is_null($result)) {
    if (isset($wgSMEDebug) && $wgSMEDebug) {
    $restr = print_r($result, true);

    file_put_contents("/local/sme/msg_sent_to_kernel.txt", "UDPSOCK CALL: $sql_str",
        FILE_APPEND | LOCK_EX);
    file_put_contents("/local/sme/msg_sent_to_kernel.txt", "UDPSOCK RESULT: $restr",
        FILE_APPEND | LOCK_EX);
    }
    if (!is_bool($result)) {
      $result->close();
    }
  } else {
    if (isset($wgSMEDebug) && $wgSMEDebug) {
      file_put_contents("/local/sme/msg_sent_to_kernel.txt", "RESULT EMPTY!\n",
        FILE_APPEND | LOCK_EX);
    }
  }
}


function sendBackendProfileToKernel($pubhash, $privhash, $mysqli)
{
  global $SME_MARKER_SOCK_OR_CTL;
  global $wgSMEDebug;
  global $wgSMEDummyIP, $wgSMEDummyPort;
  global $SME_KERNEL_ENFORCEMENT_TIMESTAMP;
  global $wgSMEStats;
  global $wgSMEIOCTL;

  try {
    if (isset($wgSMEDummyIP))
      $dummy_server_ip = $wgSMEDummyIP;
    else
      $dummy_server_ip = "139.19.168.173";

    if (isset($wgSMEDummyPort))
      $dummy_server_port = $wgSMEDummyPort;
    else
      $dummy_server_port = 2231;

    get_local_ip_port($server_ip, $server_port);
    get_remote_ip_port($client_ip, $client_port);

    if (isset($wgSMEStats) && $wgSMEStats) {
      $time_start = microtime(true);
    }

    identify_backend_connection($local_backend_ip, $local_backend_port,
      $remote_backend_ip, $remote_backend_port);

    if (isset($wgSMEStats) && $wgSMEStats) {
      $time_end = microtime(true);
      $time = $time_end - $time_start;
      file_put_contents("/local/sme/stats_5_tuple.txt",
        "$time\n",
        LOCK_EX|FILE_APPEND);

    }

    /*
    file_put_contents("/local/sme/besock.txt",
      "src: $client_ip $client_port, in: $server_ip $server_port" .
      ", out: $local_backend_ip, $local_backend_port" .
      ", dst: $remote_backend_ip, $remote_backend_port\n", FILE_APPEND|LOCK_EX);
     */

    $num_pub = count($pubhash);
    $num_priv = count($privhash);


    // BE-FE profile, to be forwarded to the backend via SQL
    create_tmp_profile_marker($pbuf_BE_FE, $plen_BE_FE,
      $local_backend_ip, $local_backend_port,
      $remote_backend_ip, $remote_backend_port,
      $remote_backend_ip, $remote_backend_port,
      $local_backend_ip, $local_backend_port,
      $num_pub, $num_priv, $pubhash, $privhash);

    sendBackendProfileToBackendMysql($pbuf_BE_FE, $plen_BE_FE, $mysqli,
      $dummy_server_ip, $dummy_server_port);

    // FE-BE profile
    create_tmp_profile_marker($pbuf_FE_BE, $plen_FE_BE,
      $client_ip, $client_port,
      $server_ip, $server_port,
      $local_backend_ip, $local_backend_port,
      $remote_backend_ip, $remote_backend_port,
      $num_pub, $num_priv, $pubhash, $privhash);

    /*
    $sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
     */
    
    // Wait for ACK to the FE-Client marker on the frontend kernel,
    // before sending the FE-BE marker
    // Only in the case of UDP SOCKETS
    if (isset($wgSMEIOCTL) && $wgSMEIOCTL==0 && isset($wgSMEDebug) && $wgSMEDebug == 0) {
      waitForKernelACK($SME_MARKER_SOCK_OR_CTL, $dummy_server_ip, $dummy_server_port);
    }

    // Send FE-BE profile marker to kernel 
    if (isset($wgSMEIOCTL) && $wgSMEIOCTL) {
      $ret = dev_ioctl($SME_MARKER_SOCK_OR_CTL, 0, $pbuf_FE_BE, $plen_FE_BE);
    } else {
      $ret = socket_sendto($SME_MARKER_SOCK_OR_CTL, $pbuf_FE_BE, $plen_FE_BE, 0,
        $dummy_server_ip, $dummy_server_port);
    }


    if (isset($wgSMEIOCTL) && $wgSMEIOCTL==0 && isset($wgSMEDebug) && $wgSMEDebug == 0) {
      waitForKernelACK($SME_MARKER_SOCK_OR_CTL, $dummy_server_ip, $dummy_server_port);
    }
  }
  catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
  }
  finally {
    if (isset($wgSMEIOCTL) && $wgSMEIOCTL) {
      dev_close($SME_MARKER_SOCK_OR_CTL);
      #echo "Sent BE, closing ioctl dev";
    } else {
      socket_close($SME_MARKER_SOCK_OR_CTL);
      #echo "Sent BE, closing socket";
    }
  }

}


/*
 * Wait for kernel acknowledgement to our UDP msg.
 * The acknowledgement buffer contains a single uint64_t timestamp value.
 * Update the global var $SME_KERNEL_ENFORCEMENT_TIMESTAMP with this value.
 */
function waitForKernelACK($sock, $dummy_server_ip, $dummy_server_port){
  global $SME_KERNEL_ENFORCEMENT_TIMESTAMP;
  global $wgSMEDebug;
  global $wgSMEStats;
  if ($sock == -1) {
    file_put_contents("/local/sme/msg_sent_to_kernel.txt", "sock: $sock\n",
      FILE_APPEND | LOCK_EX);
    return;
  }

  if (isset($wgSMEStats) && $wgSMEStats) {
    $time_start = microtime(true);
  }
  
  $ret = socket_recvfrom($sock, $ackbuf, 8, 0,
    $dummy_server_ip, $dummy_server_port);
  
  if (isset($wgSMEStats) && $wgSMEStats) {
    $time_end = microtime(true);
    $time = $time_end - $time_start;
    file_put_contents("/local/sme/stats_marker_sync.txt",
      "$time\n",
      LOCK_EX|FILE_APPEND);
  }


  $SME_KERNEL_ENFORCEMENT_TIMESTAMP = int_helper::uInt64($ackbuf);

  if (isset($wgSMEDebug) && $wgSMEDebug) {
    file_put_contents("/local/sme/msg_sent_to_kernel.txt",
      "RETURN VALUE: $ret, Kernel TS Value: $SME_KERNEL_ENFORCEMENT_TIMESTAMP\n",
      LOCK_EX|FILE_APPEND);
  }
}

function sendProfileToKernel($pubhash, $privhash)
{
  
  global $SME_MARKER_SOCK_OR_CTL;
  global $wgSMEMultiTier;
  global $wgSMEioctlDEV;
  global $wgSMEIOCTL;
  global $wgSMEDebug;
  global $wgSMEDummyIP, $wgSMEDummyPort;
  global $SME_KERNEL_ENFORCEMENT_TIMESTAMP;
  try {
    if (isset($wgSMEDummyIP))
      $dummy_server_ip = $wgSMEDummyIP;
    else
      $dummy_server_ip = '139.19.168.173';

    if (isset($wgSMEDummyPort))
      $dummy_server_port = $wgSMEDummyPort;
    else
      $dummy_server_port = 2231;

    get_local_ip_port($server_ip, $server_port);
    get_remote_ip_port($client_ip, $client_port);

    #print_r($pubhash);
    $num_pub = count($pubhash);
    $num_priv = count($privhash);

    create_tmp_profile_marker($pbuf, $plen, $client_ip, $client_port,
      $server_ip, $server_port, $server_ip, $server_port,
      $client_ip, $client_port, $num_pub, $num_priv, $pubhash, $privhash);

    if (isset($wgSMEIOCTL) && $wgSMEIOCTL) {
      #echo "Opening IOCTL $wgSMEioctlDEV: ";
      $SME_MARKER_SOCK_OR_CTL = dev_open($wgSMEioctlDEV);
      #echo $SME_MARKER_SOCK_OR_CTL;
    } else {
      $SME_MARKER_SOCK_OR_CTL = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
    }
    /*
    apache_note('SME_SIZE', '500');
    apache_note('SME_TIME', '0.5');
    apache_note('SME_START_TIME', microtime(true));
    #apache_note('SME_SIZE', '7000');
    #apache_note('SME_TIME', '0.3');
    #apache_note('SME_START_TIME', $_SERVER['REQUEST_TIME_FLOAT']);
     */

    if (isset($wgSMEIOCTL) && $wgSMEIOCTL) {
      #echo "\nUsing IOCTL: ";
      $ret = dev_ioctl($SME_MARKER_SOCK_OR_CTL, 0, $pbuf, $plen);
      #echo $ret;
    } else {
      #echo "Not Using IOCTL";
      $ret = socket_sendto($SME_MARKER_SOCK_OR_CTL, $pbuf, $plen, 0,
        $dummy_server_ip, $dummy_server_port);
    }
  }
  catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
  }
  finally {
    if (isset($wgSMEMultiTier) && $wgSMEMultiTier == 0) {
      if (isset($wgSMEIOCTL) && $wgSMEIOCTL) {
        dev_close($SME_MARKER_SOCK_OR_CTL);
        #echo "closing ioctl dev";
      } else {
        #echo "closing socket";
        socket_close($SME_MARKER_SOCK_OR_CTL);
      }
    }
  }

  /*
  if (isset($wgSMEDebug) && $wgSMEDebug == 1){ 
    socket_close($SME_MARKER_SOCK_OR_CTL);
  }
   */

  /*
  if (isset($wgSMEDebug) && $wgSMEDebug) {
    file_put_contents("/local/sme/udp.txt", "UDP SOCK SEND (2) ret: $ret\n",
      FILE_APPEND|LOCK_EX);
  }
   */
 
  /*
  if (isset($wgSMEDebug) && $wgSMEDebug) {
    file_put_contents("/local/sme/udp.txt", "UDP SOCK RECV (2) ret: $ret\n",
      FILE_APPEND|LOCK_EX);
  }
   */

}
